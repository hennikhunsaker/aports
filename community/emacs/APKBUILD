# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
# Contributor: Timo Teräs <timo.teras@iki.fi>
pkgname=emacs
pkgver=28.2
pkgrel=9
pkgdesc="Extensible, customizable, self-documenting real-time display editor (common files)"
arch="all"
url="https://www.gnu.org/software/emacs/emacs.html"
license="GPL-3.0-or-later"
makedepends="
	!libexecinfo-dev
	alsa-lib-dev
	autoconf
	automake
	fontconfig-dev
	gawk
	giflib-dev
	glib-dev
	gmp-dev
	gnutls-dev
	gtk+3.0-dev
	harfbuzz-dev
	jansson-dev
	libgccjit-dev
	libjpeg-turbo-dev
	libpng-dev
	librsvg-dev
	libxaw-dev
	libxml2-dev
	libxpm-dev
	linux-headers
	ncurses-dev
	pango-dev
	tiff-dev
	"
subpackages="
	$pkgname-doc
	$pkgname-nox
	$pkgname-gtk3
	$pkgname-gtk3-nativecomp:gtk3_nativecomp
	$pkgname-x11
	$pkgname-x11-nativecomp:x11_nativecomp
	"
source="https://ftp.gnu.org/gnu/emacs/emacs-$pkgver.tar.xz
	fix-desktop-quoted-email.patch
	fix-desktop-quoted-email-2.patch
	no-git-repo.patch
	CVE-2022-45939.patch
	"

# secfixes:
#   28.2-r6:
#     - CVE-2023-27986
#   28.2-r5:
#     - CVE-2023-27985
#   28.2-r3:
#     - CVE-2022-45939

prepare() {
	default_prepare
	./autogen.sh
	rm etc/emacs.service # systemd unit

	mkdir -p nox
	mv ./* nox || true

	cp -a nox x11
	cp -a nox x11-nativecomp
	cp -a nox gtk3
	cp -a nox gtk3-nativecomp
}

_build_variant() {
	cd "$builddir/$1"
	case "$1" in
	x11-nativecomp|gtk3-nativecomp)
		_extra="NATIVE_FULL_AOT=1"
		;;
	esac
	shift
	CFLAGS="$CFLAGS -O2 -flto=auto" \
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--libexecdir=/usr/lib \
		--localstatedir=/var \
		--with-gameuser=:games \
		--with-gpm \
		--with-harfbuzz \
		--with-json \
		"${@}"

	make $_extra
}

_build_x11() {
	_build_variant x11 \
		--with-x-toolkit=athena \
		--without-toolkit-scroll-bars \
		--without-dbus \
		--with-xft \
		--with-jpeg=yes \
		--with-tiff=yes
}

_build_x11_nativecomp() {
	_build_variant x11-nativecomp \
		--with-x-toolkit=athena \
		--without-toolkit-scroll-bars \
		--without-dbus \
		--with-xft \
		--with-jpeg=yes \
		--with-tiff=yes \
		--with-native-compilation \
		--without-compress-install
}

_build_gtk3() {
	 _build_variant gtk3 \
		--with-x-toolkit=gtk3 \
		--with-xft \
		--with-jpeg=yes \
		--with-tiff=no
}

_build_gtk3_nativecomp() {
	 _build_variant gtk3-nativecomp \
		--with-x-toolkit=gtk3 \
		--with-xft \
		--with-jpeg=yes \
		--with-tiff=no \
		--with-native-compilation
}

_build_nox() {
	_build_variant nox \
		--without-sound \
		--without-x \
		--without-file-notification
}

build() {
	_build_nox
	_build_x11
	_build_x11_nativecomp
	_build_gtk3
	_build_gtk3_nativecomp
}

package() {
	make DESTDIR="$pkgdir" install -C gtk3

	cd "$pkgdir"
	# resolve conflict with ctags package
	mv usr/bin/ctags \
		usr/bin/ctags.emacs
	mv usr/share/man/man1/ctags.1.gz \
		usr/share/man/man1/ctags.emacs.1.gz

	# remove things that are per-subpackage and not common
	rm -r \
		usr/bin/emacs \
		usr/bin/emacs-$pkgver \
		usr/lib/emacs/$pkgver/*/emacs.pdmp \
		usr/lib/systemd \
		usr/share/emacs/$pkgver/lisp \
		usr/share/emacs/$pkgver/site-lisp

	# fix perms on /var/games
	chmod 775 var/games
	chmod 775 var/games/emacs
	chmod 664 var/games/emacs/*
	chown -R root:games var/games

	# fix user/root permissions on usr/share files
	find usr/share/emacs/ -exec chown root:root {} \;
	find usr/lib -perm -g+s,g+x ! -type d -exec chmod g-s {} \;
}

_subpackage() {
	cd "$builddir/$1"
	make DESTDIR="$subpkgdir" install

	cd "$subpkgdir"

	# remove things that are in -common
	rm -rf \
		usr/bin/ctags \
		usr/bin/ebrowse \
		usr/bin/etags \
		usr/bin/emacsclient \
		usr/include \
		usr/lib/emacs/$pkgver/*/hexl \
		usr/lib/emacs/$pkgver/*/movemail \
		usr/lib/emacs/$pkgver/*/rcs2log \
		usr/lib/emacs/$pkgver/*/update-game-score \
		usr/lib/systemd \
		usr/share/applications \
		usr/share/emacs/$pkgver/etc \
		usr/share/icons \
		usr/share/info \
		usr/share/man \
		usr/share/metainfo \
		var/games

	# fix user/root permissions on usr/share files
	find usr/share/emacs/ -exec chown root:root {} \;
	find usr/lib -perm -g+s,g+x ! -type d -exec chmod g-s {} \;
}

nox() {
	pkgdesc="$pkgdesc - non-graphical"
	depends="
		$pkgname=$pkgver-r$pkgrel
		!emacs-gtk3
		!emacs-gtk3-nativecomp
		!emacs-x11
		!emacs-x11-nativecomp
		"
	_subpackage nox
}

x11() {
	pkgdesc="$pkgdesc - with X11"
	depends="
		$pkgname=$pkgver-r$pkgrel
		!emacs-gtk3
		!emacs-gtk3-nativecomp
		!emacs-nox
		!emacs-x11-nativecomp
		desktop-file-utils
		hicolor-icon-theme
		"
	_subpackage x11
}

x11_nativecomp() {
	pkgdesc="$pkgdesc - with X11 and native compilation"
	depends="
		$pkgname=$pkgver-r$pkgrel
		!emacs-gtk3
		!emacs-gtk3-nativecomp
		!emacs-nox
		!emacs-x11
		desktop-file-utils
		hicolor-icon-theme
		"
	_subpackage x11-nativecomp
}

gtk3() {
	pkgdesc="$pkgdesc - with GTK3"
	depends="
		$pkgname=$pkgver-r$pkgrel
		!emacs-gtk3-nativecomp
		!emacs-nox
		!emacs-x11
		!emacs-x11-nativecomp
		desktop-file-utils
		hicolor-icon-theme
		"
	_subpackage gtk3
}

gtk3_nativecomp() {
	pkgdesc="$pkgdesc - with GTK3 and native compilation"
	depends="
		$pkgname=$pkgver-r$pkgrel
		!emacs-gtk3
		!emacs-nox
		!emacs-x11
		!emacs-x11-nativecomp
		desktop-file-utils
		hicolor-icon-theme
		"
	_subpackage gtk3-nativecomp
}

sha512sums="
a7cec7e3e82367815a1442f69af54102dbfc434069810a9dec5938a6660cb8b076e6f1fb0bfff9695b15603dbbe05eb9c7dfd92e90cf40fc4d1e5746bce83bd8  emacs-28.2.tar.xz
278dc6b339b68f39e78125674dada55053586dbfdbdac60d9521e60f863ff7b2bb426287cbeb20b3d5f7c8833453542335310f057642e20fc5643b6d948c13cb  fix-desktop-quoted-email.patch
3ca86a7b1ac8c1619cae93e88d39ab4fa10fdf49f04801f322a87e2e26054e64095606efaadb0880e6631fecbdf7cff6b3e926334325c2a2431f49be8f81cf18  fix-desktop-quoted-email-2.patch
5a2d1d10d66fd335b16d9f2d6d1e45e093067db87f8ef2d14a8c921cc94b3e226310c7cc691d19eb91eaf16de9ae4f20ff8be3596a4906b0514fa5f098328d97  no-git-repo.patch
ee9adf79d86f422853efa3fabb63a644bd68c54f6348d1a64418b01f33575dee48a71eb83d8b7b43b26703914824fd0b46e2d2e0d7499fd649da49fe70792e9f  CVE-2022-45939.patch
"
