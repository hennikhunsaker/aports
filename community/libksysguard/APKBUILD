# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=libksysguard
pkgver=5.27.6
pkgrel=0
pkgdesc="KDE system monitor library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="LGPL-2.1-or-later AND (GPL-2.0-only OR GPL-3.0-only)"
depends_dev="
	kauth-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	libcap-dev
	libnl3-dev
	libpcap-dev
	lm-sensors-dev
	plasma-framework-dev
	qt5-qttools-dev
	qt5-qtwebchannel-dev
	zlib-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/libksysguard-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

replaces="ksysguard<5.22"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/libksysguard.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# processtest requires working OpenGL
	xvfb-run ctest --test-dir build --output-on-failure -E "processtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
06f059baf7ae7e283b31a052cccf91a2982ddea4de1490da69de46709c4ccbdb1eae972408751045806a1e67e4dc4c945131d2006f7d69fc5af64d73a22113cd  libksysguard-5.27.6.tar.xz
"
