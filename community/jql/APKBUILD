# Contributor: Jakub Jirutka <jakub@jirutka.cz>
# Maintainer: Jakub Jirutka <jakub@jirutka.cz>
pkgname=jql
pkgver=7.0.2
pkgrel=0
pkgdesc="A JSON Query Language CLI tool"
url="https://github.com/yamafaktory/jql"
arch="all"
license="MIT"
makedepends="cargo cargo-auditable"
source="https://github.com/yamafaktory/jql/archive/jql-v$pkgver.tar.gz"
options="net" # fetch dependencies
builddir="$srcdir/jql-jql-v$pkgver"

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --frozen --release
}

check() {
	# core::tests::get_property_as_index seems to be broken
	cargo test --frozen -- \
		--skip core::tests::get_property_as_index
}

package() {
	install -D -m755 target/release/jql -t "$pkgdir"/usr/bin/
}

sha512sums="
126243c536cbb384590076863d71fa3821d30e45b22bf83ed69da743dbe8007a520f7b5b5c1cc44b889e7172899b6880615ea587204deb1276eba022d6bca39a  jql-v7.0.2.tar.gz
"
