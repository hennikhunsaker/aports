# Contributor: Francesco Camuffo <dev@fmac.xyz>
# Maintainer: Francesco Camuffo <dev@fmac.xyz>
pkgname=ugrep
pkgver=3.12.4
pkgrel=0
pkgdesc="Ultra fast grep with interactive query UI and fuzzy search"
url="https://github.com/Genivia/ugrep/wiki"
arch="all"
license="BSD-3-Clause"
checkdepends="bash"
makedepends="bzip2-dev lz4-dev pcre2-dev xz-dev zlib-dev zstd-dev"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/Genivia/ugrep/archive/refs/tags/v$pkgver.tar.gz"

build() {
	CXXFLAGS="$CXXFLAGS -O2 -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man
	make
}

check() {
	make test
}

package() {
	make DESTDIR="$pkgdir" install
}

sha512sums="
bc9e79d9e1a13fe9298439ab05104a70d6944dd4177b571af4934d5803bd443bcce69d233b1a89dd8a6bd09f5105224d03742f7e9104716b00e3af3a70f06c18  ugrep-3.12.4.tar.gz
"
