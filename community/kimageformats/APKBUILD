# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=kimageformats
pkgver=5.108.0
pkgrel=1
pkgdesc="Image format plugins for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	libavif-dev
	libheif-dev
	libraw-dev
	openexr-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kimageformats-$pkgver.tar.xz"

case "$CARCH" in
s390x)
	;;
*)
	makedepends="$makedepends libjxl-dev"
	;;
esac

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/frameworks/kimageformats.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKIMAGEFORMATS_HEIF=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure -E "kimageformats-read-psd"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
df43867153cecd876fd48fd8c9cbfe78016ed25fe6e1f074f4f395e4c0bf9f62c7bfbe3d40f79fca591d0e07324c4f58de214594a23c227e07bb33cf7f2805f0  kimageformats-5.108.0.tar.xz
"
