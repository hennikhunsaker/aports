# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kscreen
pkgver=5.27.6
pkgrel=0
pkgdesc="KDE's screen management software"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="hicolor-icon-theme"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	layer-shell-qt-dev
	libkscreen-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsensors-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kscreen-$pkgver.tar.xz"
subpackages="$pkgname-lang"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/kscreen.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# kscreen-kded-configtest is broken
	# kscreen-kded-osdtest hangs
	xvfb-run ctest --test-dir build --output-on-failure -E "kscreen-kded-(config|osd)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
130546f2a8d3033b79a0552720cd2265bee21bde209cfc07b4b012acbb023523803467864e87c720e695a31e1cbe9038514deb6f5d223f3f251bcc5823c1cc65  kscreen-5.27.6.tar.xz
"
