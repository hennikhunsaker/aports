# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=oxygen-sounds
pkgver=5.27.6
pkgrel=0
pkgdesc="The Oxygen Sound Theme"
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/oxygen-sounds-$pkgver.tar.xz"

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/oxygen-sounds.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
025a546426dd7a83bac8a604ae53cb58580f2b15d63f136c3f9d346fccceae8c48e8520a92052a281dc5ddcc298fda728a24d00f8d1fb56163939268fae7ad71  oxygen-sounds-5.27.6.tar.xz
"
