# Contributor: Daniel Isaksen <d@duniel.no>
# Contributor: Victor Schroeder <me@vschroeder.net>
# Contributor: Marlus Saraiva <marlus.saraiva@gmail.com>
# Maintainer: Michal Jirků <box@wejn.org>
pkgname=elixir
pkgver=1.15.4
pkgrel=0
pkgdesc="Elixir is a dynamic, functional language designed for building scalable and maintainable applications"
url="https://elixir-lang.org/"
# arm: build fails hundreds of times in a row on builders
# x86: fails tests
arch="noarch !x86 !armhf !armv7"
license="Apache-2.0"
depends="erlang-dev>=23.0"
makedepends="erlang-dialyzer"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/elixir-lang/elixir/archive/v$pkgver.tar.gz
	"

build() {
	LANG="en_US.UTF-8" make
}

check() {
	set +e
	make test
	ret=$?
	set -e

	# test starts epmd, which then hangs presubmit pipeline.
	# so we kill it here.
	killall -q epmd

	return $ret
}

package() {
	make DESTDIR="$pkgdir" PREFIX=/usr install
}

sha512sums="
46d30fd8cce79ca88d66602cf5eaaea4c877871ca06ad5a87494892f412685e1bd3d1bac547474fcf40f325c45583f1feabde86962e30a6d7626e7f9bdee2bd6  elixir-1.15.4.tar.gz
"
