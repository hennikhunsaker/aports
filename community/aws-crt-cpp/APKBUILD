# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=aws-crt-cpp
pkgver=0.20.6
pkgrel=0
pkgdesc="C++ wrapper around the aws-c-* libraries. Provides Cross-Platform Transport Protocols and SSL/TLS implementations for C++"
url="https://github.com/awslabs/aws-crt-cpp"
# s390x: aws-c-common
# arm*, ppc64le: aws-c-io
arch="all !armhf !armv7 !ppc64le !s390x"
license="Apache-2.0"
depends_dev="
	$pkgname=$pkgver-r$pkgrel
	aws-c-auth-dev
	aws-c-cal-dev
	aws-c-common-dev
	aws-c-compression-dev
	aws-c-event-stream-dev
	aws-c-http-dev
	aws-c-io-dev
	aws-c-mqtt-dev
	aws-c-s3-dev
	aws-c-sdkutils-dev
	aws-checksums-dev
	s2n-tls-dev
	"
makedepends="
	$depends_dev
	cmake
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-crt-cpp/archive/refs/tags/v$pkgver.tar.gz"
options="net"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja\
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_DEPS=False \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j${JOBS:-2}
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	# just test binaries
	# shellcheck disable=2115
	rm -rf "$pkgdir"/usr/bin/
}

dev() {
	default_dev
	amove usr/lib/aws-crt-cpp
}

sha512sums="
f089a0b0a8050cacc2943abea6dbc4f6d276e22178b91ef6de0d82b7dc66dcc6489641edbf4d6398f01a19f15d86a3a9934d5a81867bc048893662fbe10f55d2  aws-crt-cpp-0.20.6.tar.gz
"
