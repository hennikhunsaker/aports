# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=plasma-nano
pkgver=5.27.6
pkgrel=0
pkgdesc="A minimal Plasma shell package intended for embedded devices"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/kde/plasma-nano"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="qt5-qtvirtualkeyboard"
makedepends="
	extra-cmake-modules
	kwayland-dev
	kwindowsystem-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/plasma-nano-$pkgver.tar.xz"
options="!check" # No tests

_commit=""
snapshot() {
	clean
	makedepends="git xz rsync tar" deps
	mkdir -p "$srcdir" && cd "$srcdir"
	git clone --filter=tree:0 https://invent.kde.org/plasma/plasma-nano.git .
	git archive --format tar --prefix=$pkgname/ $_commit > ../$pkgname-$_commit.tar
	xz -vv -T0 -9 -e ../$pkgname-$_commit.tar
}

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
934792ffe5223552d01bcea1d99ed0c099e4719ffb1b8fc49ba5e19239185d4926fc5d24d2f150bf1daaa6048909229d77da37b5d4fc8e4c8366e9361f99e94a  plasma-nano-5.27.6.tar.xz
"
