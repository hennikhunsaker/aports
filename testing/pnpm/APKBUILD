# Contributor: Hygna <hygna@proton.me>
# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=pnpm
pkgver=8.6.8
pkgrel=0
pkgdesc="Fast, disk space efficient package manager"
url="https://pnpm.io"
arch="noarch"
license="MIT"
depends="nodejs"
source="https://registry.npmjs.org/pnpm/-/pnpm-$pkgver.tgz"
options="!check" # not implemented
builddir="$srcdir/package"

prepare() {
	default_prepare

	# remove node-gyp
	rm -rf dist/node-gyp-bin dist/node_modules/node-gyp
	# remove windows files
	rm -rf dist/vendor/*.exe

	# remove other unnecessary files
	find . -type f \( \
		-name '.*' -o \
		-name '*.cmd' -o \
		-name '*.bat' -o \
		-name '*.map' -o \
		-name '*.md' -o \
		-iname 'LICENSE*' -o \
		-iname 'README*' \) -delete
}

package() {
	local DESTDIR="$pkgdir"/usr/share/node_modules/pnpm

	mkdir -p "$DESTDIR"
	cp -R "$builddir"/* "$DESTDIR"/

	mkdir -p "$pkgdir"/usr/bin
	ln -sf ../share/node_modules/pnpm/bin/pnpm.cjs "$pkgdir"/usr/bin/pnpm
	ln -sf ../share/node_modules/pnpm/bin/pnpx.cjs "$pkgdir"/usr/bin/pnpx
}

sha512sums="
3adac245413b56a4722a5cbd13781f1316473801ed143a66ba5c9514aae7977bff9f6c4972476e795ba6589a94039147c5c44ffd3e36afc3fc083afd76e3cff7  pnpm-8.6.8.tgz
"
