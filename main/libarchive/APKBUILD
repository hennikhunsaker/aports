# Contributor: Sergei Lukin <sergej.lukin@gmail.com>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libarchive
pkgver=3.7.0
pkgrel=0
pkgdesc="library that can create and read several streaming archive formats"
url="https://libarchive.org/"
arch="all"
license="BSD-2-Clause AND BSD-3-Clause AND Public-Domain"
makedepends="
	acl-dev
	attr-dev
	bsd-compat-headers
	bzip2-dev
	expat-dev
	lz4-dev
	openssl-dev
	xz-dev
	zlib-dev
	zstd-dev
	"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc $pkgname-tools"
source="https://libarchive.org/downloads/libarchive-$pkgver.tar.xz"

# secfixes:
#   3.6.1-r2:
#     - CVE-2022-36227
#   3.6.1-r0:
#     - CVE-2022-26280
#   3.6.0-r0:
#     - CVE-2021-36976
#   3.4.2-r0:
#     - CVE-2020-19221
#     - CVE-2020-9308
#   3.4.0-r0:
#     - CVE-2019-18408
#   3.3.2-r1:
#     - CVE-2017-14166

build() {
	CFLAGS="$CFLAGS -ffat-lto-objects -flto=auto" \
	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--without-xml2
	make
}

package() {
	make DESTDIR="$pkgdir" install
}

tools() {
	pkgdesc="libarchive tools bsdtar and bsdcpio"

	amove usr/bin
}

sha512sums="
f69ff7fbec7e909b6a03dd5b01c47316f95a277907409c8fba3930bb90d02cd9a329921eada59ca1afc9a19e34de7eb34e9d535bbc8cd98fb586f723bd0fdba8  libarchive-3.7.0.tar.xz
"
